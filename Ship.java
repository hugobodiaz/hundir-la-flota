import java.util.ArrayList;
import java.util.List;

record position(
int x, int y
){
    boolean SamePlace(position p)
    {
        if(p==this)
            return true;

        if(p.x() == x && p.y() == y)
            return true;

        return false;
    }

    boolean SamePlace(int arg_x, int arg_y)
    {

        if(arg_x == x && arg_y == y)
            return true;

        return false;
    }

}

enum type_of_ship{
t_portaaviones,
t_submarino,
t_destructor,
t_fragatas
}

public class Ship {
    type_of_ship type;
    int length;
    int lives;
    char sprite;
    boolean alive = true;
    String name;
    List<position> positions = new ArrayList<position>();
    
    public boolean isAlive() {
        return alive;
    }

    void CreateSpaces(int x, int y, boolean is_horizontal)
    {
        int h_multi=0;
        int v_multi=0;
        if(is_horizontal)
        {
            h_multi=1;
            v_multi=0;
        }
        else
        {
            h_multi=0;
            v_multi=1;
        }

        for (int i = 0; i <= length-1; ++i) {
            positions.add(new position(x+i*h_multi,y+i*v_multi));

        }
    }
}

class portaaviones extends Ship{

    portaaviones(){
        type = type_of_ship.t_portaaviones;
        length = 4;
        lives = length;
        sprite = 'H';
        name = "Porta aviones";
    }
}

class submarino extends Ship{

    submarino(){
        type = type_of_ship.t_submarino;
        length = 3;
        lives = length;
        sprite = 'S';
        name = "Submarino";
    }

}

class destructor extends Ship{

    destructor(){
        type = type_of_ship.t_destructor;
        length = 2;
        lives = length;
        sprite = 'D';
        name = "Destructor";
    }


}

class fragata extends Ship{

    fragata(){
        type = type_of_ship.t_destructor;
        length = 1;
        lives = length;
        sprite = 'F';
        name = "Fragata";
    }

}