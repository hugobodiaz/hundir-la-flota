import java.util.List;
import java.util.ArrayList;
enum players{
        human,
        CPU
    }

enum BombReturn{
    NOT,
    WATER,
    HIT,
    SINK
}

public class Battleships_board {
    
    static int size_x = 10;
    static int size_y = 10;
    char[][] board = new char[10][10];

    List<Ship> ships = new ArrayList<Ship>();
    List<position> bombed_locations = new ArrayList<position>();

    players player;

    Battleships_board()
    {}

    Battleships_board(players p)
    {
        player = p;
        start_board();

        ships.add(new portaaviones());
        ships.add(new submarino());
        ships.add(new submarino());
        ships.add(new submarino());
        ships.add(new destructor());
        ships.add(new destructor());
        ships.add(new destructor());
        ships.add(new fragata());
        ships.add(new fragata());


    }

    public void draw_board()
    {
        for(int i=0;i<size_y;++i)
        {
            String linea="";
            for(int j=0;j<size_x;++j)
            {
                char to_print = board[j][i];

                linea+=Utilities.GetColor(to_print);

                linea += to_print + " ";

                linea+=Utilities.ANSI_RESET;
                
            }
            
            System.out.println(linea);
        }
        System.out.println("\n");
    }

    public void start_board()
    {
        for(int i=0;i<size_y;++i)
        {
            for(int j=0;j<size_x;++j)
            {
                board[j][i]= '-';
            }
        }
    }

    public BombReturn bomb(int x, int y)
    {
        BombReturn r = BombReturn.NOT;
        if(!HasBeenShot(x, y))
        {
            r= BombReturn.WATER;

            board[x][y] = 'X';
            bombed_locations.add(new position(x,y));

            for (Ship s : ships) {
                for (position p_ship : s.positions) {
                    if(p_ship.SamePlace(x,y))
                    {
                        s.lives-=1;

                        if(s.lives<=0)
                            r=BombReturn.SINK;
                        else
                            r=BombReturn.HIT;
                    }
                }
            }

        }
        return r;
    }

    public boolean isAlive()
    {
        boolean ret = false;

        for (Ship s : ships) {
            if(s.lives>0)
                ret = true;
        }

        return ret;
    }

    public void RenderToBoard()
    {
        //empezamos desde 0?
        //start_board();
        //añadimos barquitos
        RenderShipsToBoard();
        //añadimos sitios que han bombardeado
        RenderExplosionsToBoard();
    }   

    public void RenderExplosionsToBoard()
    {
        for (position p : bombed_locations) {
            board[p.x()][p.y()] = 'X';
            for (Ship cs : ships) {
                for (position p_ship : cs.positions) {
                    if(p_ship.SamePlace(p))
                    {
                        board[p.x()][p.y()] = 'O';
                        if(cs.lives<=0)
                        {
                            board[p.x()][p.y()] = '0';
                        }
                    }
                }
            }
        }
    }
    public void RenderShipsToBoard()
    {
        for (Ship cs : ships) {
            for (position p : cs.positions) {
                board[p.x()][p.y()] = cs.sprite;
            }
        }
    }

    public void autofill()
    {
        for (Ship it : ships) {

            boolean valid_position = false;
            while(!valid_position)
            {
                //System.out.println("Posicionar: " + it.name);
                boolean horizontal = Utilities.RandomBoolean();
                
                int x_range = 9;
                int y_range = 9;

                if(horizontal)
                    x_range-=it.length;
                else
                    y_range-= it.length;

                int posx = Utilities.RandomInt(0,x_range);
                int posy = Utilities.RandomInt(0,y_range);

                if(isPositionEmpty(posx,posy,it.length,horizontal))
                {
                    it.CreateSpaces(posx,posy,horizontal);
                    valid_position=true;
                }

            }
        }
    }

    public boolean isPositionEmpty(int x, int y, int length, boolean horizontal)
    {
        boolean ret = true;

        int h_multi=0;
        int v_multi=0;
        if(horizontal)
        {
            h_multi=1;
            v_multi=0;
        }
        else
        {
            h_multi=0;
            v_multi=1;
        }

        for (int i = 0; i <= length-1; i++)
        {
            for (Ship shp : ships) 
            {
                for (position p : shp.positions)
                {
                    if(p.x() == x+i*h_multi && p.y() ==y+i*v_multi )
                    {
                        ret = false;
                    }
                }
            }
        }

        return ret;
    }


    public boolean HasBeenShot(int x, int y)
    {
        boolean ret = false;
        for (position p : bombed_locations) {
            if(p.SamePlace(x, y))
            {
                ret = true;
                break;
            }
        }
        return ret;
    }
}
