import java.io.Console;


public class Main {

    static Game game;

    public static void main(String[] args) {

        game = new Game();

        System.out.println("------Bienvenido a 'Hundir la flota!'------");
        game.start_game();
        System.out.println("Ahora pasamos a la fase de disparos, en la que tomas turnos para intentar adivinar donde esta la flota enemiga");
        
        game.computer.start_board();
        while(!Game.isGameOver())
        {
            System.out.println("Turno del jugador");
            game.Turn(false);
            game.computer.RenderExplosionsToBoard();
            
            System.out.println("Turno de la máquina");
            game.Turn(true);
            game.player.RenderToBoard();
            game.DrawBoards();

            game.getGameState();
        }


        switch (game.getGameState()) {
            case PLAYER_WIN:
            System.out.println("Has derrotado a la flota enemiga, buen trabajo!!");
                break;
            case CPU_WIN:
            System.out.println("Oh no!, han destruido todos tus navíos!");
                break;
            default:
                break;
        }

    }

}
