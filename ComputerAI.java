import java.util.List;

enum ComputerState{
    RANDOM_SELECT,
    NEIGHBOURS_SEARCH,
    DIRECTION_TRACKING
}

public class ComputerAI {

    //variables for neighbours search
    int x_start;
    int y_start;
    boolean[] discovered = new boolean[4];//LEFT -> UP -> RIGHT -> DOWN

    //variables for direction tracking
    boolean horizontal;

    ComputerState state = ComputerState.RANDOM_SELECT;

    public void Shoot(Game game)
    {

        int pos_x;
        int pos_y;
        BombReturn r = BombReturn.NOT;
        switch (state) {
            case RANDOM_SELECT:

            do{

                pos_x = Utilities.RandomInt(0, 9);
                pos_y = Utilities.RandomInt(0, 9);

                r = game.player.bomb(pos_x, pos_y);
                } while ( r == BombReturn.NOT);
                
                if(r == BombReturn.HIT)
                {
                    x_start = pos_x;
                    y_start = pos_y;
                    state = ComputerState.NEIGHBOURS_SEARCH;
                }

            break;
            case NEIGHBOURS_SEARCH:

            int dir; 

            do {
                dir = Utilities.RandomInt(0, 3);

                pos_x = x_start;
                pos_y = y_start;

                switch (dir) {
                    case 0:
                        pos_x-=1;
                        break;
                    case 1:
                        pos_y-=1;
                        break;
                    case 2:
                        pos_x+=1;
                        break;
                    case 3:
                        pos_y+=1;
                        break;
                }

                pos_x = keep_number_inbounds(pos_x, 0, 9);
                pos_y = keep_number_inbounds(pos_y, 0, 9);

                r = game.player.bomb(pos_x, pos_y);

                if(game.player.HasBeenShot(x_start - 1, y_start) && 
                game.player.HasBeenShot(x_start + 1, y_start) &&
                game.player.HasBeenShot(x_start, y_start - 1)  &&
                game.player.HasBeenShot(x_start, y_start + 1))
                {
                    state = ComputerState.RANDOM_SELECT;
                    break;
                }


            } while ( r == BombReturn.NOT);

            if(r == BombReturn.HIT)
            {

                if(dir == 0 || dir ==2)
                    horizontal = true;
                else if(dir ==1 || dir == 3)
                    horizontal = false;

                state = ComputerState.DIRECTION_TRACKING;
            }
            break;
            case DIRECTION_TRACKING:
            int offset;
            do {
                offset = Utilities.RandomInt(0,6);

                pos_x = x_start;
                pos_y = y_start;
                if(horizontal)
                    pos_x +=offset-3;
                else
                    pos_y+=offset-3;

                pos_x = keep_number_inbounds(pos_x, 0, 9);
                pos_y = keep_number_inbounds(pos_y, 0, 9);

                r = game.player.bomb(pos_x, pos_y);

                int hor_fact;
                int ver_fact;
                if(horizontal)
                {
                    hor_fact = 1;
                    ver_fact = 0;
                }
                else
                {
                    hor_fact = 0;
                    ver_fact = 1;
                }

                if(game.player.HasBeenShot(x_start -3*hor_fact, y_start -3*ver_fact) && 
                game.player.HasBeenShot(x_start -2*hor_fact, y_start -2*ver_fact) &&
                game.player.HasBeenShot(x_start -1*hor_fact, y_start -1*ver_fact) &&
                game.player.HasBeenShot(x_start, y_start) &&
                game.player.HasBeenShot(x_start +1*hor_fact, y_start +1*ver_fact) &&
                game.player.HasBeenShot(x_start +2*hor_fact, y_start +2*ver_fact) &&
                game.player.HasBeenShot(x_start +3*hor_fact, y_start +3*ver_fact))
                {
                    state = ComputerState.RANDOM_SELECT;
                    break;
                }


            } while ( r == BombReturn.NOT);

            break;
        
            default:
            state = ComputerState.RANDOM_SELECT;
                break;
        }


        if(r == BombReturn.SINK)
        {
            state = ComputerState.RANDOM_SELECT;
        }
    }


    int keep_number_inbounds(int input,int lowest, int highest)
    {
        int ret = input;

        if(input > highest)
            ret = highest;
        
        if(input < lowest)
            ret = lowest;

        return ret;

    }

    
}
