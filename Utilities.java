import java.io.*;
import java.util.Random;




enum ConsoleColor{
    BLACK,
    RED,
    GREEN,
    YELLOW,
    BLUE,
    PURPLE,
    CYAN,
    WHITE
}



public abstract class Utilities {


    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";


    public static boolean isParsableInt(String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (final NumberFormatException e) {
            return false;
        }
    }
    public static boolean isParsableDouble(String input) {
        try {
            Double.parseDouble(input);
            return true;
        } catch (final NumberFormatException e) {
            return false;
        }
    }
    public static int readInt(Console cnsl, String errormsg)
    {
        String s = cnsl.readLine();

        while(!Utilities.isParsableInt(s))
        {
            System.out.println(errormsg);
            s = cnsl.readLine();
        }

        return Integer.parseInt(s);

    }

    public static int readIntRange(Console cnsl, int min, int max)
    {
        int ret = 0;

        boolean exit = false;
        while(!exit)
        {
            int i =readInt(cnsl, "Por favor usa un número!");
            if (i>=min &&i<=max )
            {
                ret = i;
                exit = true;
            }

            if(i<min)
                System.out.println("Ese número es muy pequeño");
            if(i>max)
                System.out.println("Ese número es muy grande");
        }

        return ret;
    }

    public static Double readDouble(Console cnsl, String errormsg)
    {
        String s = cnsl.readLine();

        while(!Utilities.isParsableDouble(s))
        {
            System.out.println(errormsg);
            s = cnsl.readLine();
        }

        Double d=Double.parseDouble(s);

        return (double)Math.round(d*100)/100;

    }

    public static boolean readBinaryResponse(Console cnsl, String question)
    {
        //System.out.println("Quieres rellenar manualmente tu tablero?('si' o 'no')");
        System.out.println(question + "('si' o 'no')");

        boolean ret = false;
        boolean exit = false;
        while(!exit)
        {
            String respuesta = cnsl.readLine();
            switch (respuesta) {
                case "si":
                    ret= true;
                    exit = true;
                    break;
                case "no":
                    ret = false;
                    exit = true;
                    break;
                default:
                System.out.println("Respuesta no valida, prueba con 'si' o 'no'");
                    break;
            }

        }
        return ret;
    }


    public static int RandomInt(int min, int max)
    {
        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
        
    }

    public static boolean RandomBoolean()
    {
        Random r = new Random();
        return r.nextBoolean();
    }


    public static String GetColouredString(String s, ConsoleColor c)
    {
        return c+s+ANSI_RESET;
    }



    public static String GetColor(char c)
    {
        String ret = "";
        switch (c) {
            case 'X':
                ret+=Utilities.ANSI_CYAN;
                break;
            case '0':
                ret+=Utilities.ANSI_BLACK;
                break;
            case 'O':
                ret+=Utilities.ANSI_RED;
                break;
            case 'D':
                ret+=Utilities.ANSI_PURPLE;
                break;
            case 'H':
                ret+=Utilities.ANSI_PURPLE;
                break;
            case 'S':
                ret+=Utilities.ANSI_PURPLE;
                break;
            case 'F':
                ret+=Utilities.ANSI_PURPLE;
                break;
            case '-':
                ret+=Utilities.ANSI_BLUE;
                break;
        }
        return ret;
    }
}