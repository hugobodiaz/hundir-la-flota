import java.io.*;

enum GameState{
    PLAYER_WIN,
    CPU_WIN,
    ONGOING
}


public class Game {
    static Battleships_board player;
    static Battleships_board computer;

    static boolean game_over=false;

    static ComputerAI AI;

    public void Turn(boolean CPU)
    {
        boolean has_shot = false;
        Console cnsl = System.console();


        int pos_x;
        int pos_y;


        if (CPU) {
            AI.Shoot(this);
            /*
            do {
                
                pos_x = Utilities.RandomInt(0, 9);
                pos_y = Utilities.RandomInt(0, 9);
            } while (player.bomb(pos_x, pos_y) == BombReturn.NOT);*/
        } else {
            BombReturn r = BombReturn.NOT;
            do {
                System.out.println("posicion x(0 - 9)");
                pos_x = Utilities.readIntRange(cnsl,0,9);
                System.out.println("posicion y(0 - 9)");
                pos_y = Utilities.readIntRange(cnsl,0,9);

                r=computer.bomb(pos_x, pos_y);

                if(r==BombReturn.NOT)
                {
                    System.out.println("Esa posición ya ha sido bombardeada previamente!");
                }

            } while (r == BombReturn.NOT);

            has_shot=true;
        }
        

    }

    public static boolean isGameOver()
    {  
        return game_over;
    }



    public static void DrawBoards()
    {
        System.out.println("TABLERO DEL JUGADOR \t\t TABLERO DE LA MÁQUINA");

        for(int i=0;i<player.size_y;++i)
        {
            String linea="";
            for(int j=0;j<player.size_x;++j)
            {
                char to_print = player.board[j][i];
                linea+=Utilities.GetColor(to_print);

                linea += player.board[j][i] + " ";

                linea+=Utilities.ANSI_RESET;

            }

            linea+="\t\t";

            for(int j=0;j<computer.size_x;++j)
            {
                char to_print = computer.board[j][i];
                linea+=Utilities.GetColor(to_print);

                linea += computer.board[j][i] + " ";

                linea+=Utilities.ANSI_RESET;
            }
            
            System.out.println(linea);
        }
    }

    public static GameState getGameState()
    {
        if (!player.isAlive()) {
            game_over=true;
            return GameState.CPU_WIN;
        }
        if (!computer.isAlive()) {
            game_over=true;
            return GameState.PLAYER_WIN;
        }
        else{
            return GameState.ONGOING;
        }
    }

    public static void start_game()
    {
        
        player = new Battleships_board(players.human);
        computer = new Battleships_board(players.CPU);

        computer.autofill();
        AI = new ComputerAI();

        Console cnsl = System.console();
        
        if(!Utilities.readBinaryResponse(cnsl,"Quieres rellenar manualmente tu tablero?"))
            player.autofill();
        else
        {
            System.out.println("------Rellena tu tablero!------");
            for (Ship it : player.ships) {


                boolean valid_position = false;
                while(!valid_position)
                {
                    System.out.println("Posicionar barco "+ player.ships.indexOf(it)+": " + it.name);
                    boolean horizontal = Utilities.readBinaryResponse(cnsl,"Quieres posicionar horizontalmente el barco?");
                    
                    int x_range = 9;
                    int y_range = 9;
    
                    if(horizontal)
                        x_range-=it.length;
                    else
                        y_range-= it.length;
    
                    System.out.println("posicion x(0 - "+x_range+")");
                    int posx = Utilities.readIntRange(cnsl,0,x_range);
                    System.out.println("posicion y(0 - "+y_range+")");
                    int posy = Utilities.readIntRange(cnsl,0,y_range);
    
                    if(player.isPositionEmpty(posx,posy,it.length,horizontal))
                    {
                        it.CreateSpaces(posx,posy,horizontal);
                        valid_position=true;

                        player.RenderToBoard();
                        player.draw_board();
                    }
                    else
                    {
                        System.out.print("Ese lugar esta ocupado por otros barcos!");
                    }
    
                }

            }
        }
        player.RenderToBoard();
        DrawBoards();



        /*System.out.print("COMPUTER BOARD\n");
        computer.RenderToBoard();
        computer.draw_board();

        System.out.print("PLAYER BOARD\n");
        player.RenderToBoard();
        player.draw_board();*/



    }
}
